// background script

let tabCount = null;


browser.tabs.onCreated.addListener(tab => {
    if (tabCount === null) {
        browser.tabs.query({}, tabs => {
            tabCount = tabs.length;
            browser.browserAction.setBadgeText({"text": tabCount.toString()});
        });
    } else {
        tabCount++;
        browser.browserAction.setBadgeText({"text": tabCount.toString()});
    }
});


browser.tabs.onRemoved.addListener(tab => {
    if (tabCount === null) {
        browser.tabs.query({}, tabs => {
            tabCount = tabs.length;
            browser.browserAction.setBadgeText({"text": tabCount.toString()});
        });
    } else {
        tabCount--;
        browser.browserAction.setBadgeText({"text": tabCount.toString()});
    }
});


browser.runtime.onInstalled.addListener(details =>
    browser.tabs.query({}, tabs => {
        tabCount = tabs.length;
        browser.browserAction.setBadgeText({"text": tabCount.toString()});
    })
);

(callback => {
    if (document.readyState === "loading") {
        document.addEventListener("DOMContentLoaded", callback);
    } else {
        callback();
    }
})(() => {  // main
    // console.time("tabCalc");
    const allTabsPromise = new Promise(resolve =>
        browser.tabs.query({}, tabs => resolve(tabs))
    );
    const curWindowPromise = new Promise(resolve =>
        browser.windows.getCurrent(window => resolve(window))
    );
    const globalTabCountSpan = document.getElementById("globalTabCount");
    const windowCountSpan = document.getElementById("windowCount");
    const curWindowTabCountSpan = document.getElementById("curWindowTabCount");
    Promise.all([allTabsPromise, curWindowPromise]).then(([allTabs, curWindow]) => {
        const curWindowId = curWindow.id;
        let tabsInCurWindow = 0;
        const windowIDs = new Set();
        for (const tab of allTabs) {
            const tabWindowId = tab.windowId;
            windowIDs.add(tabWindowId);
            if (tabWindowId === curWindowId) {
                tabsInCurWindow++;
            }
        }
        globalTabCountSpan.textContent = allTabs.length.toString();
        windowCountSpan.textContent = windowIDs.size.toString();
        curWindowTabCountSpan.textContent = tabsInCurWindow.toString();
        // console.timeEnd("tabCalc");
    });

    const exportButton = document.getElementById("exportbutton");
    exportButton.onclick = () => {
        browser.permissions.request({permissions: ["tabs"]}, granted => {
            if (!granted) {
                console.log("permission not granted");
                return;
            }
            browser.tabs.query({}, async allTabs => {
                const tabsJSON = {};
                for (const tab of allTabs) {
                    const tabWindowId = tab.windowId;
                    if (!tabsJSON.hasOwnProperty(tabWindowId)) {
                        tabsJSON[tabWindowId] = [];
                    }
                    tabsJSON[tabWindowId].push(tab.url);
                }
                const { saveFile } = await import("./lib.js");
                saveFile(JSON.stringify(tabsJSON), "firefox_tabs.json", "application/json");
            });
        });
    };
    exportButton.style.color = "blue";
    exportButton.style.textDecoration = "underline";
    exportButton.style.cursor = "pointer";
});

#!/usr/bin/env python3
import json
import os
import shutil
from string import Template
import traceback

import cairosvg


def render_icons(input_fpath, build_dir):
    icons_dir = os.path.join(build_dir, 'icons')
    os.mkdir(icons_dir)
    for size in (48, 96):
        cairosvg.svg2png(
            url=input_fpath,
            write_to=os.path.join(icons_dir, '{}.png'.format(size)),
            output_height=size
        )
    os.rename(input_fpath, os.path.join(icons_dir, 'icon.svg'))


def build():
    print('\nBuild\n========')

    # read metadata JSON
    print('Reading metadata.json...', end='')
    with open('metadata.json') as f:
        metadata = json.load(f)
    version = metadata['version']
    print(' done.')

    # copy directory over
    build_dir = f'firefox-tab-counter-{version}'
    print('Creating {}...'.format(build_dir), end='')
    shutil.copytree('firefox-tab-counter', build_dir)
    print(' done.')

    # detemplating
    print('Detemplating files...')
    template_mapping = {
        'version': version
    }
    for root, _, files in os.walk(build_dir):
        for fname in files:
            if fname.endswith('.template'):
                fpath = os.path.join(root, fname)
                new_fpath = os.path.join(root, fname[:-len('.template')])
                print('  Detemplating {}...'.format(new_fpath), end='')
                with open(fpath) as f:
                    content = f.read()
                detemplated_content = Template(content).substitute(template_mapping)
                with open(new_fpath, 'w') as f:
                    f.write(detemplated_content)
                os.remove(fpath)
                print(' done.')
    print('done.')

    print('Rendering icons...', end='')
    render_icons(
        input_fpath=os.path.join(build_dir, 'icon.svg'),
        build_dir=build_dir
    )
    print(' done.')

    # print('Zipping {}...'.format(build_dir))
    # shutil.make_archive(build_dir, 'zip', base_dir=build_dir)
    # print(' done.')
    #
    # print('Removing {}...'.format(build_dir), end='')
    # if os.path.isdir(build_dir):
    #     shutil.rmtree(build_dir)
    # print(' done.')


def clean():
    print('\nClean\n========')

    print('Reading metadata.json...', end='')
    with open('metadata.json') as f:
        metadata = json.load(f)
    version = metadata['version']
    print(' done.')

    build_dir = f'firefox-tab-counter-{version}'
    if os.path.isdir(build_dir):
        print('Removing {}...'.format(build_dir), end='')
        shutil.rmtree(build_dir)
        print(' done.')

    if os.path.isfile(build_dir + '.zip'):
        print('Removing {}...'.format(build_dir + '.zip'), end='')
        os.remove(build_dir + '.zip')
        print(' done.')


def main():
    clean()
    try:
        build()
    except Exception:
        traceback.print_exc()
        clean()


if __name__ == '__main__':
    main()
